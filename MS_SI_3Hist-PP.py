#!/usr/bin/env python
# coding: utf-8
# In[30]:
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
matplotlib.use('Agg')
import sklearn
from sklearn.linear_model import LinearRegression
import seaborn as sns; sns.set(style="ticks", color_codes=True)
from scipy.integrate import odeint

kB = 8.617E-5
T=298.15
#-----------------------------------------
#PBE
C2H4_ads_PBE1 = -887.2347096 + 1.34
C2H4_ads_PBE2 = -919.2696946 + 2*(1.34)
C2H4_ads_PBE3 = -952.0451751 + 3*(1.34)
C2H4_free_PBE = -31.9634663 + 1.39 - 0.678
H2O_ads_PBE1 = -869.3338647 + 0.629
H2O_ads_PBE2 = -884.10898 + 2*(0.629)
H2O_ads_PBE3 = -898.8088181 + 3*(0.629)
H2O_free_PBE = -14.2209464 + 0.603 - 0.584
zeolite_PBE = -854.10658
#-----------------------------------------
#HSE
C2H4_ads_HSE1 = -1211.12274 + 1.34
C2H4_ads_HSE2 = -1249.249413 + 2*(1.34)
C2H4_ads_HSE3 = -1288.034834 + 3*(1.34)
C2H4_free_HSE = -38.07780615 +1.39 - 0.678
H2O_ads_HSE1 = -1191.798217 + 0.629
H2O_ads_HSE2 = -1211.214083 + 2*(0.629)
H2O_ads_HSE3 = -1230.591463 + 3*(0.629)
H2O_free_HSE = -18.87323798 + 0.603 - 0.584
zeolite_HSE = -1171.9988
#---------------------------------------------------------------------------------------
# ************* C2H4 AND H2O***************
C2H4_H2O_PBE = -902.0079807 + 0.512
C2H4_H2O_HSE = -1230.557244 + 0.512
C2H4_H2O_BEEFvdw = np.genfromtxt('BEEF_vdw_C2H4-H2O.csv') + 0.674 - 0.512
# ***********************************************
#----------------------------------------------------------------------------------------
#BEEFvdw
C2H4_1_egy = np.genfromtxt('BEEF_vdw_C2H4-1.csv') + 1.43 - 1.34
C2H4_2_egy = np.genfromtxt('BEEF_vdw_C2H4-2.csv') + 0.528 - 0.301
C2H4_3_egy = np.genfromtxt('BEEF_vdw_C2H4-3.csv') + 0.279 - (-0.00861)
C2H4_free_np = np.genfromtxt('BEEF_vdw_ensemble_energies_C2H4_free.csv') + 1.3886 - 0.67770
H2O_1_egy = np.genfromtxt('BEEF_vdw_H2O-1.csv') + 0.682 - 0.629
H2O_2_egy = np.genfromtxt('BEEF_vdw_H2O-2.csv') + 1.35 - 1.24
H2O_3_egy = np.genfromtxt('BEEF_vdw_H2O-3.csv') + 0.0896 - (-0.073)
H2O_free_np = np.genfromtxt('BEEF_vdw_ensemble_energies_H2O_free.csv') + 0.6034 - 0.58352
zeolite_np = np.genfromtxt('BEEF_vdw_Zeolite.csv')
#-----------------------------------------------------------------------------------------#y_energies is delta G
y_energies = np.zeros((len(C2H4_1_egy),4))
y_energies[:,0] = C2H4_1_egy - C2H4_free_np - zeolite_np
y_energies[:,1] = C2H4_2_egy - C2H4_1_egy - C2H4_free_np
y_energies[:,2] = C2H4_H2O_BEEFvdw - H2O_1_egy - C2H4_free_np
y_energies[:,3] = C2H4_3_egy - C2H4_2_egy - C2H4_free_np
C2H4_eng = np.concatenate(y_energies).reshape((2000,4))

y2_energies = np.zeros((len(H2O_1_egy),4))
y2_energies[:,0] = H2O_1_egy - H2O_free_np - zeolite_np
y2_energies[:,1] = H2O_2_egy - H2O_1_egy - H2O_free_np
y2_energies[:,2] = C2H4_H2O_BEEFvdw - C2H4_1_egy - H2O_free_np
y2_energies[:,3] = H2O_3_egy - H2O_2_egy -  H2O_free_np
H2O_eng = np.concatenate(y2_energies).reshape((2000,4))

#----C2H4----
#PBE
y_energiesPBE = np.zeros((50,4))
y_energiesPBE[:,0] = C2H4_ads_PBE1 - C2H4_free_PBE - zeolite_PBE
y_energiesPBE[:,1] = C2H4_ads_PBE2 - C2H4_ads_PBE1 - C2H4_free_PBE
y_energiesPBE[:,2] = C2H4_H2O_PBE - H2O_ads_PBE1 - C2H4_free_PBE
y_energiesPBE[:,3] = C2H4_ads_PBE3 - C2H4_ads_PBE2 - C2H4_free_PBE
#y_energiesPBE = np.concatenate(y_energiesPBE).reshape((50,4))
#-----------
#HSE
y_energiesHSE = np.zeros((50,4))
y_energiesHSE[:,0] = C2H4_ads_HSE1 - C2H4_free_HSE - zeolite_HSE
y_energiesHSE[:,1] = C2H4_ads_HSE2 - C2H4_ads_HSE1 - C2H4_free_HSE
y_energiesHSE[:,2] = C2H4_H2O_HSE - H2O_ads_HSE1 - C2H4_free_HSE
y_energiesHSE[:,3] = C2H4_ads_HSE3 - C2H4_ads_HSE2 - C2H4_free_HSE
#y_energiesHSE = np.concatenate(y_energiesHSE).reshape((50,4)) 
#----H2O----
#PBE
y2_energiesPBE = np.zeros((50,4))
y2_energiesPBE[:,0] = H2O_ads_PBE1 - H2O_free_PBE - zeolite_PBE
y2_energiesPBE[:,1] = H2O_ads_PBE2 - H2O_ads_PBE1 - H2O_free_PBE
y2_energiesPBE[:,2] = C2H4_H2O_PBE - C2H4_ads_PBE1 - H2O_free_PBE
y2_energiesPBE[:,3] = H2O_ads_PBE3 - H2O_ads_PBE2 - H2O_free_PBE
#-----------
#HSE
y2_energiesHSE = np.zeros((50,4))
y2_energiesHSE[:,0] = H2O_ads_HSE1 - H2O_free_HSE - zeolite_HSE
y2_energiesHSE[:,1] = H2O_ads_HSE2 - H2O_ads_HSE1 - H2O_free_HSE
y2_energiesHSE[:,2] = C2H4_H2O_HSE - C2H4_ads_HSE1 - H2O_free_HSE
y2_energiesHSE[:,3] = H2O_ads_HSE3 - H2O_ads_HSE2 - H2O_free_HSE


#----------------------------------------------------------------------
# 3rd Histogram 

B_eng3 = y_energies[:,3]
P_eng3 = y_energiesPBE[:,3]
H_eng3 = y_energiesHSE[:,3]
B_eng3np = np.array(B_eng3)
P_eng3np = np.array(P_eng3)
H_eng3np = np.array(H_eng3)

B2_eng3 = y2_energies[:,3]
P2_eng3 = y2_energiesPBE[:,3]
H2_eng3 = y2_energiesHSE[:,3]
B2_eng3np = np.array(B2_eng3)
P2_eng3np = np.array(P2_eng3)
H2_eng3np = np.array(H2_eng3)

B_eng3np.flatten()
P_eng3np.flatten()
H_eng3np.flatten()
B2_eng3np.flatten()
P2_eng3np.flatten()
H2_eng3np.flatten()

#------------------
#Plot
preset_bins = np.linspace(-2,1,25)
fig, ax = plt.subplots()
ax.hist([B_eng3np,P_eng3np,H_eng3np], bins=preset_bins, label=['$BEEF_{vdw}$','PBE','HSE'], histtype='barstacked')
ax.set_title('$C_2H_4$ Adsorption Energies',  fontsize=18)
ax.set_ylabel('Count', fontsize=18)
ax.set_xlabel('Adsorption Energy (eV)', fontsize=18)
ax.set_xlim(-2.5,2)
ax.set_ylim(0,300)
plt.legend(loc='upper right', fontsize=16)
fig.savefig('SI_Hist_3C2H4.png', dpi=220)

fig, ax = plt.subplots()
ax.hist([B2_eng3np,P2_eng3np,H2_eng3np], bins=preset_bins, label=['$BEEF_{vdw}$','PBE','HSE'], histtype='barstacked')
ax.set_title('$H_2O$ Adsorption Energies', fontsize=18)
ax.set_ylabel('Count', fontsize=18)
ax.set_xlabel('Adsorption Energy (eV)', fontsize=18)
ax.set_xlim(-2,2)
ax.set_ylim(0,600)
plt.legend(loc='upper right', fontsize=16)
fig.savefig('SI_Hist_3H2O.png', dpi=220)

#-----------------------------------------------------------------------
#Supporting Info PairPlot

DFT2 = np.vstack((C2H4_eng, y_energiesPBE))
DFT3 = np.vstack((DFT2, y_energiesHSE))
#This is where i will specify the 2 columns that i want in the pairplot
DFT3_2col = DFT3[:,1:3]    #old for the first 2 columns --> [:,:2]
index0 = np.zeros((2000,1))
a = np.array([[1],[2]])
index1 = np.repeat(a, 50)
index1 = np.reshape(index1, (100, 1))
index = np.vstack((index0, index1))
DFT_all = np.hstack((DFT3_2col,index))
C2H4pd_eng = pd.DataFrame(np.array(DFT_all), columns=[ r'$C_2H_4^* + C_2H_4(g) \leftrightarrow 2C_2H_4^*$',r'$C_2H_4^* + H_2O(g) \leftrightarrow (C_2H_4+H_2O)^*$', 'DFT Type'])
#----------------------
sns.set(style="ticks", font_scale=1)
C2H4_H2O_PairPlot = sns.pairplot(C2H4pd_eng, vars=[r'$C_2H_4^* + C_2H_4(g) \leftrightarrow 2C_2H_4^*$',r'$C_2H_4^* + H_2O(g) \leftrightarrow (C_2H_4+H_2O)^*$'], hue="DFT Type",  markers=["o", "s", "x"])
C2H4_H2O_PairPlot.set(xlim=(-3,2))
C2H4_H2O_PairPlot.set(ylim=(-3,2))
C2H4_H2O_PairPlot = C2H4_H2O_PairPlot.add_legend(fontsize=20)
C2H4_H2O_PairPlot.savefig('SI_Pairplot.png', dpi=220)


