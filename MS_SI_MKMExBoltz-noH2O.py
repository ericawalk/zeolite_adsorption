#!/usr/bin/env python
# coding: utf-8
# In[30]:
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
matplotlib.use('Agg')
import sklearn
from sklearn.linear_model import LinearRegression
import seaborn as sns; sns.set(style="ticks", color_codes=True)
from scipy.integrate import odeint
import statsmodels.formula.api as smf


kB = 8.617E-5
T=298.15
H2O_free = pd.read_csv('BEEF_vdw_ensemble_energies_H2O_free.csv', header=None) + 0.6034 - 0.58352 #+ZPE - entropy
C2H4_free = pd.read_csv('BEEF_vdw_ensemble_energies_C2H4_free.csv', header=None) + 1.3886 - 0.67770
H2O_ads = pd.read_csv('BEEF_vdw_H2O-1.csv', header=None) + 0.682 - 0.629
C2H4_ads = pd.read_csv('BEEF_vdw_C2H4-1.csv', header=None) + 1.43 - 1.34
zeolite = pd.read_csv('BEEF_vdw_Zeolite.csv', header=None)
#-----------------------------------------
#PBE
C2H4_ads_PBE1 = -887.2347096 + 1.34
C2H4_ads_PBE2 = -919.2696946 + 2*(1.34)
C2H4_ads_PBE3 = -952.0451751 + 3*(1.34)
C2H4_free_PBE = -31.9634663 + 1.39 - 0.678

H2O_ads_PBE1 = -869.3338647 + 0.629
H2O_ads_PBE2 = -884.10898 + 2*(0.629)
H2O_ads_PBE3 = -898.8088181 + 3*(0.629)
H2O_free_PBE = -14.2209464 + 0.603 - 0.584

zeolite_PBE = -854.10658
#-----------------------------------------
#HSE
C2H4_ads_HSE1 = -1211.12274 + 1.34
C2H4_ads_HSE2 = -1249.249413 + 2*(1.34)
C2H4_ads_HSE3 = -1288.034834 + 3*(1.34)
C2H4_free_HSE = -38.07780615 +1.39 - 0.678

H2O_ads_HSE1 = -1191.798217 + 0.629
H2O_ads_HSE2 = -1211.214083 + 2*(0.629)
H2O_ads_HSE3 = -1230.591463 + 3*(0.629)
H2O_free_HSE = -18.87323798 + 0.603 - 0.584

zeolite_HSE = -1171.9988
#---------------------------------------------------------------------------------------
# ************* C2H4 AND H2O***************
C2H4_H2O_PBE = -902.0079807 + 0.512
C2H4_H2O_HSE = -1230.557244 + 0.512
C2H4_H2O_BEEFvdw = np.genfromtxt('BEEF_vdw_C2H4-H2O.csv') + 0.674 - 0.512
# ***********************************************
#----------------------------------------------------------------------------------------
#BEEFvdw
C2H4_1_egy = np.genfromtxt('BEEF_vdw_C2H4-1.csv') + 1.43 - 1.34
C2H4_2_egy = np.genfromtxt('BEEF_vdw_C2H4-2.csv') + 0.528 - 0.301
C2H4_3_egy = np.genfromtxt('BEEF_vdw_C2H4-3.csv') + 0.279 - (-0.00861)
C2H4_free_np = np.genfromtxt('BEEF_vdw_ensemble_energies_C2H4_free.csv') + 1.3886 - 0.67770

H2O_1_egy = np.genfromtxt('BEEF_vdw_H2O-1.csv') + 0.682 - 0.629
H2O_2_egy = np.genfromtxt('BEEF_vdw_H2O-2.csv') + 1.35 - 1.24
H2O_3_egy = np.genfromtxt('BEEF_vdw_H2O-3.csv') + 0.0896 - (-0.073)
H2O_free_np = np.genfromtxt('BEEF_vdw_ensemble_energies_H2O_free.csv') + 0.6034 - 0.58352

zeolite_np = np.genfromtxt('BEEF_vdw_Zeolite.csv')
#-----------------------------------------------------------------------------------------

#y_energies is delta G
y_energies = np.zeros((len(C2H4_1_egy),4))
y_energies[:,0] = C2H4_1_egy - C2H4_free_np - zeolite_np
y_energies[:,1] = C2H4_2_egy - C2H4_1_egy - C2H4_free_np 
y_energies[:,2] = C2H4_3_egy - C2H4_2_egy - C2H4_free_np
y_energies[:,3] = C2H4_H2O_BEEFvdw - H2O_1_egy - C2H4_free_np

y2_energies = np.zeros((len(H2O_1_egy),4))
y2_energies[:,0] = H2O_1_egy - H2O_free_np - zeolite_np
y2_energies[:,1] = H2O_2_egy - H2O_1_egy - H2O_free_np
y2_energies[:,2] = H2O_3_egy - H2O_2_egy -  H2O_free_np
y2_energies[:,3] = C2H4_H2O_BEEFvdw - C2H4_1_egy - H2O_free_np


y_energiesPBE = np.zeros((1,3))
y_energiesPBE[0,0] = C2H4_ads_PBE1 - C2H4_free_PBE - zeolite_PBE
y_energiesPBE[0,1] = C2H4_ads_PBE2 - C2H4_ads_PBE1 - C2H4_free_PBE
y_energiesPBE[0,2] = C2H4_H2O_PBE - H2O_ads_PBE1 - C2H4_free_PBE
y_energiesHSE = np.zeros((1,3))
y_energiesHSE[0,0] = C2H4_ads_HSE1 - C2H4_free_HSE - zeolite_HSE
y_energiesHSE[0,1] = C2H4_ads_HSE2 - C2H4_ads_HSE1 - C2H4_free_HSE
y_energiesHSE[0,2] = C2H4_H2O_HSE - H2O_ads_HSE1 - C2H4_free_HSE


C2H4_eng = np.concatenate(y_energies).reshape((2000,4))
H2O_eng = np.concatenate(y2_energies).reshape((2000,4))

#-----------Lateral Interaction Microkinetic Model-------------------------------

def microkinetic_model(y, t, C2H4_gas_frac, f1, G_1, G_4):
    theta_C2H4, theta_2C2H4, theta_vacant = y
#Equilibrium Constants
    K_1 = np.exp(-(G_1)/(kB*T))
    K_4 = np.exp(-(G_4)/(kB*T))
#Reverse Rate Constants  (forward rate constant is f)
    kr1 = f1/K_1
    kr4 = f1/K_4       #ethylene adsorbed, C2H4 already adsorbed 
#Reaction Rates
    r_rxn1 = f1*theta_vacant*C2H4_gas_frac - kr1*theta_C2H4
    r_rxn4 = f1*theta_vacant*theta_C2H4*C2H4_gas_frac - kr4*theta_2C2H4
#Microkinetic Model Equations (Mass Balances)
    d_theta_C2H4_dt = r_rxn1 - r_rxn4 
    d_theta_2C2H4_dt = r_rxn4
    d_theta_vacant_dt = -r_rxn1 - r_rxn4 
    dydt = [d_theta_C2H4_dt, d_theta_2C2H4_dt, d_theta_vacant_dt]
    return dydt

C2H4_fractions = np.zeros(len(C2H4_1_egy))
twoC2H4_fractions = np.zeros(len(C2H4_1_egy))
vacant = np.zeros(len(C2H4_1_egy))
y0 = y=np.array([0, 0 , 1])
t = np.linspace(0,1.5E10,1E4)     #0,1.5E10,1E4               #0,1E5,1E4  --  0,0.1,1E4 (for graphical abstract)
for i in range(len(C2H4_1_egy)):  
    f1 = 6.2014895E5 #adsorption rate constant ethylene on zeolite from collision theory
    G_1 = y_energies[i,0]
    G_4 = y_energies[i,1]
    theta_individual = odeint(microkinetic_model, y0, t, args=(6.0E-4, f1, G_1, G_4))    #6.0E-4, 0.06
    C2H4_fractions[i] = theta_individual[-1,0]
    twoC2H4_fractions[i] = theta_individual[-1,1]
    vacant[i] = theta_individual[-1,2]
#Plots of Microkinetic Model vs. Time For Different Adsorption Fractions
   #C2H4 100%, 2C2H4 0%
    if i == 1986:
       fig1, ax1 = plt.subplots(figsize=(5,4))
       ax1.plot(t, theta_individual[:, 0], 'b', label=r'$\theta_{C_{2}H_{4}}$', linewidth=6)
       ax1.plot(t, theta_individual[:, 1], 'r', label=r'$\theta_{2C_{2}H_{4}}$', linewidth=6)
       ax1.plot(t, theta_individual[:, 2], 'k', label=r'$\theta_{vacant}$', linewidth=6)
       ax1.legend(loc='best')
       ax1.set_xlim([-1e5,2e6])
       ax1.set_ylim([-.02,1.02])
       ax1.set_xlabel('Time (s)')
       ax1.set_ylabel(r'Adsorption Fraction')
       fig1.savefig('MicModVsTimeEx1-noH2O.png',dpi=220,bbox_inches="tight")
   #C2H4 76%, 2C2H4 12%
    elif i == 73:
       fig2, ax2 = plt.subplots(figsize=(5,4))
       ax2.plot(t, theta_individual[:, 0], 'b', label=r'$\theta_{C_{2}H_{4}}$', linewidth=6)
       ax2.plot(t, theta_individual[:, 1], 'r', label=r'2$\theta_{C_{2}H_{4}}$', linewidth=6)
       ax2.plot(t, theta_individual[:, 2], 'k', label=r'$\theta_{vacant}$', linewidth=6)
       ax2.legend(loc='best')
       ax2.set_xlim([-0.5e7,0.8e8])
       ax2.set_ylim([-.02,1.02])
       ax2.set_xlabel('Time (s)')
       ax2.set_ylabel(r'Adsorption Fraction')
       fig2.savefig('MicModVsTimeEx2-noH2O.png',dpi=220,bbox_inches="tight")
   #C2H4 60%, 2C2H4 20%
    elif i == 1996:
       fig3, ax3 = plt.subplots(figsize=(5,4))
       ax3.plot(t, theta_individual[:, 0], 'b', label=r'$\theta_{C_{2}H_{4}}$', linewidth=6)
       ax3.plot(t, theta_individual[:, 1], 'r', label=r'2$\theta_{C_{2}H_{4}}$', linewidth=6)
       ax3.plot(t, theta_individual[:, 2], 'k', label=r'$\theta_{vacant}$', linewidth=6)
       ax3.legend(loc='best')
       ax3.set_xlim([-1e6,1e7])
       ax3.set_ylim([-.02,1.02])
       ax3.set_xlabel('Time (s)')
       ax3.set_ylabel(r'Adsorption Fraction')
       fig3.savefig('MicModVsTimeEx3-noH2O.png',dpi=220,bbox_inches="tight")
   #C2H4 13%, 2C2H4 44%
    elif i == 1988:
       fig4, ax4 = plt.subplots(figsize=(5,4))
       ax4.plot(t, theta_individual[:, 0], 'b', label=r'$\theta_{C_{2}H_{4}}$', linewidth=6)
       ax4.plot(t, theta_individual[:, 1], 'r', label=r'2$\theta_{C_{2}H_{4}}$', linewidth=6)
       ax4.plot(t, theta_individual[:, 2], 'k', label=r'$\theta_{vacant}$', linewidth=6)
       ax4.legend(loc='best')
       ax4.set_xlim([-0.05e10,0.8e10])
       ax4.set_ylim([-.02,1.02])
       ax4.set_xlabel('Time (s)')
       ax4.set_ylabel(r'Adsorption Fraction')
       fig4.savefig('MicModVsTimeEx4-noH2O.png',dpi=220,bbox_inches="tight")

    #if C2H4_fractions[i] > 0.75:
    #    print("one", i)
    #if twoC2H4_fractions[i] > 0.1:
    #    print("two", i)
print("Energies")
print(y_energies[1986,0], y_energies[1986,1])
print(y_energies[73,0], y_energies[73,1])
print(y_energies[1996,0], y_energies[1996,1])
print(y_energies[1988,0], y_energies[1988,1])
print("MKM - C2H4, 2C2H4, vacant")
print(C2H4_fractions[1986], twoC2H4_fractions[1986], vacant[1986])
print(C2H4_fractions[73], twoC2H4_fractions[73], vacant[73])
print(C2H4_fractions[1996], twoC2H4_fractions[1996], vacant[1996])
print(C2H4_fractions[1988], twoC2H4_fractions[1988], vacant[1988])


# Boltzmann Distribution------------------------------
B1_1C2H4 = np.exp(-(y_energies[1986,0])/(kB*T))
B1_2C2H4 = np.exp(-(y_energies[1986,1])/(kB*T))
B2_1C2H4 = np.exp(-(y_energies[73,0])/(kB*T))
B2_2C2H4 = np.exp(-(y_energies[73,1])/(kB*T))
B3_1C2H4 = np.exp(-(y_energies[1996,0])/(kB*T))
B3_2C2H4 = np.exp(-(y_energies[1996,1])/(kB*T))
B4_1C2H4 = np.exp(-(y_energies[1988,0])/(kB*T))
B4_2C2H4 = np.exp(-(y_energies[1988,1])/(kB*T))

B_PBE_1 = np.exp(-(y_energiesPBE[0,0])/(kB*T))
B_PBE_2 = np.exp(-(y_energiesPBE[0,1])/(kB*T))
B_HSE_1 = np.exp(-(y_energiesHSE[0,0])/(kB*T))
B_HSE_2 = np.exp(-(y_energiesHSE[0,1])/(kB*T))

#totals for only Ethylene
totalBD1 = B1_1C2H4+B1_2C2H4
totalBD2 = B2_1C2H4+B2_2C2H4
totalBD3 = B3_1C2H4+B3_2C2H4
totalBD4 = B4_1C2H4+B4_2C2H4

totalPBE = B_PBE_1+B_PBE_2
totalHSE = B_HSE_1+B_HSE_2
 
#Boltzmann Prob
Boltz1C1 = B1_1C2H4/totalBD1
Boltz2C1 = B1_2C2H4/totalBD1
Boltz1C2 = B2_1C2H4/totalBD2
Boltz2C2 = B2_2C2H4/totalBD2
Boltz1C3 = B3_1C2H4/totalBD3
Boltz2C3 = B3_2C2H4/totalBD3
Boltz1C4 = B4_1C2H4/totalBD4
Boltz2C4 = B4_2C2H4/totalBD4

BoltzP1 = B_PBE_1/totalPBE
BoltzP2 = B_PBE_2/totalPBE
BoltzH1 = B_HSE_1/totalHSE
BoltzH2 = B_HSE_2/totalHSE

print("Boltzmann 1C2H4, 2C2H4")
print(Boltz1C1, Boltz2C1)
print(Boltz1C2, Boltz2C2)
print(Boltz1C3, Boltz2C3)
print(Boltz1C4, Boltz2C4)
print("PBE, HSE")
print(BoltzP1, BoltzP2)
print(BoltzH1, BoltzH2)
#---------------------------------------

C2H4_fractions = np.reshape(C2H4_fractions, (2000,1))
twoC2H4_fractions = np.reshape(twoC2H4_fractions, (2000,1))


