import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
matplotlib.use('Agg')
import sklearn
from sklearn.linear_model import LinearRegression
import seaborn as sns; sns.set(style="ticks", color_codes=True)
from scipy.integrate import odeint
import scipy.stats as st
from scipy.stats import sem, t
from scipy import mean
import pickle

kB = 8.617E-5
T=298.15
H2O_free = pd.read_csv('BEEF_vdw_ensemble_energies_H2O_free.csv', header=None) + 0.6034 - 0.58352 #+ZPE - entropy
C2H4_free = pd.read_csv('BEEF_vdw_ensemble_energies_C2H4_free.csv', header=None) + 1.3886 - 0.67770
H2O_ads = pd.read_csv('BEEF_vdw_H2O-1.csv', header=None) + 0.682 - 0.629
C2H4_ads = pd.read_csv('BEEF_vdw_C2H4-1.csv', header=None) + 1.43 - 1.34
zeolite = pd.read_csv('BEEF_vdw_Zeolite.csv', header=None)
#-----------------------------------------
#PBE
C2H4_ads_PBE1 = -887.2347096 + 1.34
C2H4_ads_PBE2 = -919.2696946 + 2*(1.34)
C2H4_ads_PBE3 = -952.0451751 + 3*(1.34)
C2H4_free_PBE = -31.9634663 + 1.39 - 0.678
H2O_ads_PBE1 = -869.3338647 + 0.629
H2O_ads_PBE2 = -884.10898 + 2*(0.629)
H2O_ads_PBE3 = -898.8088181 + 3*(0.629)
H2O_free_PBE = -14.2209464 + 0.603 - 0.584
zeolite_PBE = -854.10658
#-----------------------------------------
#HSE
C2H4_ads_HSE1 = -1211.12274 + 1.34
C2H4_ads_HSE2 = -1249.249413 + 2*(1.34)
C2H4_ads_HSE3 = -1288.034834 + 3*(1.34)
C2H4_free_HSE = -38.07780615 +1.39 - 0.678
H2O_ads_HSE1 = -1191.798217 + 0.629
H2O_ads_HSE2 = -1211.214083 + 2*(0.629)
H2O_ads_HSE3 = -1230.591463 + 3*(0.629)
H2O_free_HSE = -18.87323798 + 0.603 - 0.584
zeolite_HSE = -1171.9988
#---------------------------------------------------------------------------------------
# ************* C2H4 AND H2O***************
C2H4_H2O_PBE = -902.0079807 + 0.512
C2H4_H2O_HSE = -1230.557244 + 0.512
C2H4_H2O_BEEFvdw = np.genfromtxt('BEEF_vdw_C2H4-H2O.csv') + 0.674 - 0.512
# ***********************************************
#----------------------------------------------------------------------------------------
#BEEFvdw
C2H4_1_egy = np.genfromtxt('BEEF_vdw_C2H4-1.csv') + 1.43 - 1.34
C2H4_2_egy = np.genfromtxt('BEEF_vdw_C2H4-2.csv') + 0.528 - 0.301
C2H4_3_egy = np.genfromtxt('BEEF_vdw_C2H4-3.csv') + 0.279 - (-0.00861)
C2H4_free_np = np.genfromtxt('BEEF_vdw_ensemble_energies_C2H4_free.csv') + 1.3886 - 0.67770
H2O_1_egy = np.genfromtxt('BEEF_vdw_H2O-1.csv') + 0.682 - 0.629
H2O_2_egy = np.genfromtxt('BEEF_vdw_H2O-2.csv') + 1.35 - 1.24
H2O_3_egy = np.genfromtxt('BEEF_vdw_H2O-3.csv') + 0.0896 - (-0.073)
H2O_free_np = np.genfromtxt('BEEF_vdw_ensemble_energies_H2O_free.csv') + 0.6034 - 0.58352
zeolite_np = np.genfromtxt('BEEF_vdw_Zeolite.csv')
#-----------------------------------------------------------------------------------------
#y_energies is delta G
y_energies = np.zeros((len(C2H4_1_egy),4))
y_energies[:,0] = C2H4_1_egy - C2H4_free_np - zeolite_np
y_energies[:,1] = C2H4_2_egy - C2H4_1_egy - C2H4_free_np
y_energies[:,2] = C2H4_3_egy - C2H4_2_egy - C2H4_free_np
y_energies[:,3] = C2H4_H2O_BEEFvdw - H2O_1_egy - C2H4_free_np
y2_energies = np.zeros((len(H2O_1_egy),4))
y2_energies[:,0] = H2O_1_egy - H2O_free_np - zeolite_np
y2_energies[:,1] = H2O_2_egy - H2O_1_egy - H2O_free_np
y2_energies[:,2] = H2O_3_egy - H2O_2_egy -  H2O_free_np
y2_energies[:,3] = C2H4_H2O_BEEFvdw - C2H4_1_egy - H2O_free_np


#----C2H4----
#PBE
y_energiesPBE = np.zeros((1,4))
y_energiesPBE[0,0] = C2H4_ads_PBE1 - C2H4_free_PBE - zeolite_PBE
y_energiesPBE[0,1] = C2H4_ads_PBE2 - C2H4_ads_PBE1 - C2H4_free_PBE
y_energiesPBE[0,2] = C2H4_ads_PBE3 - C2H4_ads_PBE2 - C2H4_free_PBE
y_energiesPBE[0,3] = C2H4_H2O_PBE - H2O_ads_PBE1 - C2H4_free_PBE
#-----------
#HSE
y_energiesHSE = np.zeros((1,4))                                      
y_energiesHSE[0,0] = C2H4_ads_HSE1 - C2H4_free_HSE - zeolite_HSE     
y_energiesHSE[0,1] = C2H4_ads_HSE2 - C2H4_ads_HSE1 - C2H4_free_HSE   
y_energiesHSE[0,2] = C2H4_ads_HSE3 - C2H4_ads_HSE2 - C2H4_free_HSE   
y_energiesHSE[0,3] = C2H4_H2O_HSE - H2O_ads_HSE1 - C2H4_free_HSE
#----H2O----
#PBE
y2_energiesPBE = np.zeros((1,4))
y2_energiesPBE[0,0] = H2O_ads_PBE1 - H2O_free_PBE - zeolite_PBE
y2_energiesPBE[0,1] = H2O_ads_PBE2 - H2O_ads_PBE1 - H2O_free_PBE
y2_energiesPBE[0,2] = H2O_ads_PBE3 - H2O_ads_PBE2 - H2O_free_PBE
y2_energiesPBE[0,3] = C2H4_H2O_PBE - C2H4_ads_PBE1 - H2O_free_PBE
#-----------
#HSE
y2_energiesHSE = np.zeros((1,4))
y2_energiesHSE[0,0] = H2O_ads_HSE1 - H2O_free_HSE - zeolite_HSE
y2_energiesHSE[0,1] = H2O_ads_HSE2 - H2O_ads_HSE1 - H2O_free_HSE
y2_energiesHSE[0,2] = H2O_ads_HSE3 - H2O_ads_HSE2 - H2O_free_HSE
y2_energiesHSE[0,3] = C2H4_H2O_HSE - C2H4_ads_HSE1 - H2O_free_HSE

#----------------------------------------------------------------------------------------
y_eng = np.concatenate(y_energies).reshape((2000,4))

y_eng2 = np.vstack((y_eng, y_energiesPBE))
y_eng3 = np.vstack((y_eng2, y_energiesHSE))
#print(y_eng3)
#print(y_eng3.shape)

#--------------------------------------------------------------------------
y2_eng = np.concatenate(y2_energies).reshape((2000,4))

y2_eng2 = np.vstack((y2_eng, y2_energiesPBE))
y2_eng3 = np.vstack((y2_eng2, y2_energiesHSE))
#print(y2_eng3)
#print(y2_eng3.shape)
#-------------------------------------------------------------------------

#Surface Fractions 

def microkinetic_model(y, t, C2H4_gas_frac, H2O_gas_frac, f1, f2, G_1, G_2, G_3, G_4, G_5, G_6):
    theta_C2H4, theta_H2O, theta_2C2H4, theta_2H2O, theta_C2H4H2O, theta_vacant = y
#Equilibrium Constants
    K_1 = np.exp(-(G_1)/(kB*T))
    K_2 = np.exp(-(G_2)/(kB*T))
    K_3 = np.exp(-(G_3)/(kB*T))
    K_4 = np.exp(-(G_4)/(kB*T))
    K_5 = np.exp(-(G_5)/(kB*T))
    K_6 = np.exp(-(G_6)/(kB*T))
#Reverse Rate Constants  (forward rate constant is f)
    kr1 = f1/K_1
    kr2 = f2/K_2
    kr3 = f2/K_3       #water adsorbed, C2H4 already adsorbed    
    kr4 = f1/K_4       #ethylene adsorbed, C2H4 already adsorbed 
    kr5 = f2/K_5       #water adsorbed, H2O already adsorbed
    kr6 = f1/K_6       #ethylene adsorbed, H2O already adsorbed
#Reaction Rates
    r_rxn1 = f1*theta_vacant*C2H4_gas_frac - kr1*theta_C2H4
    r_rxn2 = f2*theta_vacant*H2O_gas_frac - kr2*theta_H2O
    r_rxn3 = f2*theta_vacant*theta_C2H4*H2O_gas_frac - kr3*theta_C2H4H2O
    r_rxn4 = f1*theta_vacant*theta_C2H4*C2H4_gas_frac - kr4*theta_2C2H4
    r_rxn5 = f2*theta_vacant*theta_H2O*H2O_gas_frac - kr5*theta_2H2O
    r_rxn6 = f1*theta_vacant*theta_H2O*C2H4_gas_frac - kr6*theta_C2H4H2O
#Microkinetic Model Equations (Mass Balances)
    d_theta_C2H4_dt = r_rxn1 - r_rxn3 - r_rxn4
    d_theta_H2O_dt = r_rxn2 - r_rxn5 - r_rxn6
    d_theta_2C2H4_dt = r_rxn4
    d_theta_2H2O_dt = r_rxn5
    d_theta_C2H4H2O_dt = r_rxn3 + r_rxn6
    d_theta_vacant_dt = -r_rxn1 - r_rxn2 - r_rxn3 - r_rxn4 - r_rxn5 - r_rxn6
    dydt = [d_theta_C2H4_dt, d_theta_H2O_dt, d_theta_2C2H4_dt, d_theta_2H2O_dt, d_theta_C2H4H2O_dt, d_theta_vacant_dt]
    return dydt

C2H4_fractions = np.zeros(len(y_eng3))
H2O_fractions = np.zeros(len(y_eng3))
twoC2H4_fractions = np.zeros(len(y_eng3))
twoH2O_fractions = np.zeros(len(y_eng3))
C2H4H2O_fractions = np.zeros(len(y_eng3))
vacant_fractions = np.zeros(len(y_eng3))

y0 = y=np.array([0, 0, 0, 0, 0 , 1])
t = np.linspace(0,1E10,1E4)
for i in range(len(y_eng3)):
    f1 = 6.2014895E5 #adsorption rate constant ethylene on zeolite from collision theory
    f2 = 7.7382354E5 #adsorption rate water from collision theory
    G_1 = y_eng3[i,0]
    G_2 = y2_eng3[i,0]
    G_3 = y2_eng3[i,3]
    G_4 = y_eng3[i,1]
    G_5 = y2_eng3[i,1]
    G_6 = y_eng3[i,3]
    theta_individual = odeint(microkinetic_model, y0, t, args=(6.0E-4, 0.06, f1, f2, G_1, G_2, G_3, G_4, G_5, G_6))
    C2H4_fractions[i] = theta_individual[-1,0]
    H2O_fractions[i] = theta_individual[-1,1]
    twoC2H4_fractions[i] = theta_individual[-1,2]
    twoH2O_fractions[i] = theta_individual[-1,3]
    C2H4H2O_fractions[i] = theta_individual[-1,4]
    vacant_fractions[i] = theta_individual[-1,5]
adsorption_fractions = np.column_stack((C2H4_fractions,H2O_fractions))
#print('adsadsorption_fractions.shape: ' + str(adsorption_fractions.shape))

C2H4_fractions = np.reshape(C2H4_fractions, (2002,1))
H2O_fractions = np.reshape(H2O_fractions, (2002,1))
twoC2H4_fractions = np.reshape(twoC2H4_fractions, (2002,1))
twoH2O_fractions = np.reshape(twoH2O_fractions, (2002,1))
C2H4H2O_fractions = np.reshape(C2H4H2O_fractions, (2002,1))
vacant_fractions = np.reshape(vacant_fractions, (2002,1))


#-------------
#Changing C2H4 Concentrations

C2H4_fractions_beta = np.zeros((len(y_eng3),4))
H2O_fractions_beta = np.zeros((len(y_eng3),4))
twoC2H4_fractions_beta = np.zeros((len(y_eng3),4))
twoH2O_fractions_beta = np.zeros((len(y_eng3),4))
C2H4H2O_fractions_beta = np.zeros((len(y_eng3),4))
vacant_fractions_beta = np.zeros((len(y_eng3),4))

for j,C2H4_fraction in enumerate(np.array([6e-4, 4.5e-4, 3e-4, 2e-4])):
    for i in range(len(y_eng3)):  
        f1 = 6.2014895E5 #adsorption rate constant ethylene on zeolite                    # from collision theory
        f2 = 7.7382354E5 #adsorption rate water from collision theory
        G_1 = y_eng3[i,0]
        G_2 = y2_eng3[i,0]
        G_3 = y2_eng3[i,3]
        G_4 = y_eng3[i,1]
        G_5 = y2_eng3[i,1]
        G_6 = y_eng3[i,3]
        #G_1 = y_eng3[i,0]
        #m_1 = M_1[i]
        #G_2 = y2_eng3[i,0]
        #m_2 = M_2[i]
        #theta_individual = odeint(microkinetic_model, y0, t, args=(C2H4_fraction, 0.06, G_1,f1,m_1,G_2,f2,m_2))
        theta_individual = odeint(microkinetic_model, y0, t, args=(C2H4_fraction, 0.06, f1, f2, G_1, G_2, G_3, G_4, G_5, G_6))
        #print(str(theta_individual[-1,:]))
        #print(theta_individual[-1,:])
        C2H4_fractions_beta[i,j] = theta_individual[-1,0]
        H2O_fractions_beta[i,j] = theta_individual[-1,1]
        twoC2H4_fractions_beta[i,j] = theta_individual[-1,2]
        twoH2O_fractions_beta[i,j] = theta_individual[-1,3]
        C2H4H2O_fractions_beta[i,j] = theta_individual[-1,4]
        vacant_fractions_beta[i,j] = theta_individual[-1,5]

print(C2H4_fractions_beta.shape)
print(twoC2H4_fractions_beta.shape)
#C2H4_fractions_beta = np.reshape(C2H4_fractions, (2002,4))
#H2O_fractions_beta = np.reshape(H2O_fractions, (2002,4))
#twoC2H4_fractions_beta = np.reshape(twoC2H4_fractions, (2002,4))
#twoH2O_fractions_beta = np.reshape(twoH2O_fractions, (2002,4))
#C2H4H2O_fractions_beta = np.reshape(C2H4H2O_fractions, (2002,4))
#vacant_fractions_beta = np.reshape(vacant_fractions, (2002,4))



#NEED TO SEPARATE PBE AND HSE *************************************
C2H4_frac_BEEF = C2H4_fractions_beta[:2000,:]
C2H4_frac_PBE = C2H4_fractions_beta[2000,:]
C2H4_frac_HSE = C2H4_fractions_beta[2001,:]

H2O_frac_BEEF = H2O_fractions_beta[:2000,:]
H2O_frac_PBE = H2O_fractions_beta[2000,:]
H2O_frac_HSE = H2O_fractions_beta[2001,:]

twoC2H4_frac_BEEF = twoC2H4_fractions_beta[:2000,:] 
twoC2H4_frac_PBE = twoC2H4_fractions_beta[2000,:]
twoC2H4_frac_HSE = twoC2H4_fractions_beta[2001,:]

twoH2O_frac_BEEF = twoH2O_fractions_beta[:2000,:]
twoH2O_frac_PBE = twoH2O_fractions_beta[2000,:]
twoH2O_frac_HSE = twoH2O_fractions_beta[2001,:]

C2H4H2O_frac_BEEF = C2H4H2O_fractions_beta[:2000,:]
C2H4H2O_frac_PBE = C2H4H2O_fractions_beta[2000,:]
C2H4H2O_frac_HSE = C2H4H2O_fractions_beta[2001,:]

vacant_frac_BEEF = vacant_fractions_beta[:2000,:]
vacant_frac_PBE = vacant_fractions_beta[2000,:]
vacant_frac_HSE = vacant_fractions_beta[2001,:]


print('PBE (C2H4, 2C2H4, H2O, 2H2O, C2H4H2O)')
print(C2H4_frac_PBE, twoC2H4_frac_PBE, H2O_frac_PBE, twoH2O_frac_PBE, C2H4H2O_frac_PBE, vacant_frac_PBE)
print('HSE')
print(C2H4_frac_HSE, twoC2H4_frac_HSE, H2O_frac_HSE, twoH2O_frac_HSE, C2H4H2O_frac_HSE, vacant_frac_HSE)




#Total C2H4 and Total H2O-----------------
tot_C2H4_singleBEEF = 0.5*C2H4_frac_BEEF
tot_C2H4_doubleBEEF = twoC2H4_frac_BEEF
tot_C2H4_hybridBEEF = 0.5*C2H4H2O_frac_BEEF
tot_C2H4_BEEF = tot_C2H4_singleBEEF + tot_C2H4_doubleBEEF + tot_C2H4_hybridBEEF
tot_H2O_singleBEEF = 0.5*H2O_frac_BEEF
tot_H2O_doubleBEEF = twoH2O_frac_BEEF
tot_H2O_hybridBEEF = 0.5*C2H4H2O_frac_BEEF
tot_H2O_BEEF = tot_H2O_singleBEEF + tot_H2O_doubleBEEF + tot_H2O_hybridBEEF

tot_C2H4_singlePBE = 0.5*C2H4_frac_PBE
tot_C2H4_doublePBE = twoC2H4_frac_PBE
tot_C2H4_hybridPBE = 0.5*C2H4H2O_frac_PBE
tot_C2H4_PBE = tot_C2H4_singlePBE + tot_C2H4_doublePBE + tot_C2H4_hybridPBE
tot_H2O_singlePBE = 0.5*H2O_frac_PBE
tot_H2O_doublePBE = twoH2O_frac_PBE
tot_H2O_hybridPBE = 0.5*C2H4H2O_frac_PBE
tot_H2O_PBE = tot_H2O_singlePBE + tot_H2O_doublePBE + tot_H2O_hybridPBE

tot_C2H4_singleHSE = 0.5*C2H4_frac_HSE
tot_C2H4_doubleHSE = twoC2H4_frac_HSE
tot_C2H4_hybridHSE = 0.5*C2H4H2O_frac_HSE
tot_C2H4_HSE = tot_C2H4_singleHSE + tot_C2H4_doubleHSE + tot_C2H4_hybridHSE
tot_H2O_singleHSE = 0.5*H2O_frac_HSE
tot_H2O_doubleHSE = twoH2O_frac_HSE
tot_H2O_hybridHSE = 0.5*C2H4H2O_frac_HSE
tot_H2O_HSE = tot_H2O_singleHSE + tot_H2O_doubleHSE + tot_H2O_hybridHSE

pickle.dump(tot_C2H4_BEEF, open("tot_C2H4_BEEF.p", "wb"))
pickle.dump(tot_H2O_BEEF, open("tot_H2O_BEEF.p", "wb"))
pickle.dump(vacant_frac_BEEF, open("vacant_frac_BEEF.p", "wb"))



