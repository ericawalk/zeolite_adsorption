#!/usr/bin/env python
# coding: utf-8
# In[30]:
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
matplotlib.use('Agg')
import sklearn
from sklearn.linear_model import LinearRegression
import seaborn as sns; sns.set(style="ticks", color_codes=True)
from scipy.integrate import odeint
import statsmodels.formula.api as smf


kB = 8.617E-5
T=298.15
H2O_free = pd.read_csv('BEEF_vdw_ensemble_energies_H2O_free.csv', header=None) + 0.6034 - 0.58352 #+ZPE - entropy
C2H4_free = pd.read_csv('BEEF_vdw_ensemble_energies_C2H4_free.csv', header=None) + 1.3886 - 0.67770
H2O_ads = pd.read_csv('BEEF_vdw_H2O-1.csv', header=None) + 0.682 - 0.629
C2H4_ads = pd.read_csv('BEEF_vdw_C2H4-1.csv', header=None) + 1.43 - 1.34
zeolite = pd.read_csv('BEEF_vdw_Zeolite.csv', header=None)
#-----------------------------------------
#PBE
C2H4_ads_PBE1 = -887.2347096 + 1.34
C2H4_ads_PBE2 = -919.2696946 + 2*(1.34)
C2H4_ads_PBE3 = -952.0451751 + 3*(1.34)
C2H4_free_PBE = -31.9634663 + 1.39 - 0.678

H2O_ads_PBE1 = -869.3338647 + 0.629
H2O_ads_PBE2 = -884.10898 + 2*(0.629)
H2O_ads_PBE3 = -898.8088181 + 3*(0.629)
H2O_free_PBE = -14.2209464 + 0.603 - 0.584

zeolite_PBE = -854.10658
#-----------------------------------------
#HSE
C2H4_ads_HSE1 = -1211.12274 + 1.34
C2H4_ads_HSE2 = -1249.249413 + 2*(1.34)
C2H4_ads_HSE3 = -1288.034834 + 3*(1.34)
C2H4_free_HSE = -38.07780615 +1.39 - 0.678

H2O_ads_HSE1 = -1191.798217 + 0.629
H2O_ads_HSE2 = -1211.214083 + 2*(0.629)
H2O_ads_HSE3 = -1230.591463 + 3*(0.629)
H2O_free_HSE = -18.87323798 + 0.603 - 0.584

zeolite_HSE = -1171.9988
#---------------------------------------------------------------------------------------
# ************* C2H4 AND H2O***************
C2H4_H2O_PBE = -902.0079807 + 0.512
C2H4_H2O_HSE = -1230.557244 + 0.512
C2H4_H2O_BEEFvdw = np.genfromtxt('BEEF_vdw_C2H4-H2O.csv') + 0.674 - 0.512
# ***********************************************
#----------------------------------------------------------------------------------------
#BEEFvdw
C2H4_1_egy = np.genfromtxt('BEEF_vdw_C2H4-1.csv') + 1.43 - 1.34
C2H4_2_egy = np.genfromtxt('BEEF_vdw_C2H4-2.csv') + 0.528 - 0.301
C2H4_3_egy = np.genfromtxt('BEEF_vdw_C2H4-3.csv') + 0.279 - (-0.00861)
C2H4_free_np = np.genfromtxt('BEEF_vdw_ensemble_energies_C2H4_free.csv') + 1.3886 - 0.67770

H2O_1_egy = np.genfromtxt('BEEF_vdw_H2O-1.csv') + 0.682 - 0.629
H2O_2_egy = np.genfromtxt('BEEF_vdw_H2O-2.csv') + 1.35 - 1.24
H2O_3_egy = np.genfromtxt('BEEF_vdw_H2O-3.csv') + 0.0896 - (-0.073)
H2O_free_np = np.genfromtxt('BEEF_vdw_ensemble_energies_H2O_free.csv') + 0.6034 - 0.58352

zeolite_np = np.genfromtxt('BEEF_vdw_Zeolite.csv')
#-----------------------------------------------------------------------------------------

#y_energies is delta G
y_energies = np.zeros((len(C2H4_1_egy),4))
y_energies[:,0] = C2H4_1_egy - C2H4_free_np - zeolite_np
y_energies[:,1] = C2H4_2_egy - C2H4_1_egy - C2H4_free_np 
y_energies[:,2] = C2H4_3_egy - C2H4_2_egy - C2H4_free_np
y_energies[:,3] = C2H4_H2O_BEEFvdw - H2O_1_egy - C2H4_free_np

y2_energies = np.zeros((len(H2O_1_egy),4))
y2_energies[:,0] = H2O_1_egy - H2O_free_np - zeolite_np
y2_energies[:,1] = H2O_2_egy - H2O_1_egy - H2O_free_np
y2_energies[:,2] = H2O_3_egy - H2O_2_egy -  H2O_free_np
y2_energies[:,3] = C2H4_H2O_BEEFvdw - C2H4_1_egy - H2O_free_np
#-------------------
C2H4_eng = np.concatenate(y_energies).reshape((2000,4))
#---------------------
H2O_eng = np.concatenate(y2_energies).reshape((2000,4))


#-----------Lateral Interaction Microkinetic Model-------------------------------

def microkinetic_model(y, t, C2H4_gas_frac, H2O_gas_frac, f1, f2, G_1, G_2, G_3, G_4, G_5, G_6):
    theta_C2H4, theta_H2O, theta_2C2H4, theta_2H2O, theta_C2H4H2O, theta_vacant = y    
#Equilibrium Constants
    K_1 = np.exp(-(G_1)/(kB*T))
    K_2 = np.exp(-(G_2)/(kB*T))
    K_3 = np.exp(-(G_3)/(kB*T))
    K_4 = np.exp(-(G_4)/(kB*T))
    K_5 = np.exp(-(G_5)/(kB*T))
    K_6 = np.exp(-(G_6)/(kB*T))
#Reverse Rate Constants  (forward rate constant is f)
    kr1 = f1/K_1
    kr2 = f2/K_2
    kr3 = f2/K_3       #water adsorbed, C2H4 already adsorbed    
    kr4 = f1/K_4       #ethylene adsorbed, C2H4 already adsorbed 
    kr5 = f2/K_5       #water adsorbed, H2O already adsorbed
    kr6 = f1/K_6       #ethylene adsorbed, H2O already adsorbed
#Reaction Rates
    r_rxn1 = f1*theta_vacant*C2H4_gas_frac - kr1*theta_C2H4
    r_rxn2 = f2*theta_vacant*H2O_gas_frac - kr2*theta_H2O
    r_rxn3 = f2*theta_vacant*theta_C2H4*H2O_gas_frac - kr3*theta_C2H4H2O
    r_rxn4 = f1*theta_vacant*theta_C2H4*C2H4_gas_frac - kr4*theta_2C2H4
    r_rxn5 = f2*theta_vacant*theta_H2O*H2O_gas_frac - kr5*theta_2H2O
    r_rxn6 = f1*theta_vacant*theta_H2O*C2H4_gas_frac - kr6*theta_C2H4H2O
#Microkinetic Model Equations (Mass Balances)
    d_theta_C2H4_dt = r_rxn1 - r_rxn3 - r_rxn4 
    d_theta_H2O_dt = r_rxn2 - r_rxn5 - r_rxn6
    d_theta_2C2H4_dt = r_rxn4
    d_theta_2H2O_dt = r_rxn5
    d_theta_C2H4H2O_dt = r_rxn3 + r_rxn6
    d_theta_vacant_dt = -r_rxn1 - r_rxn2 - r_rxn3 - r_rxn4 - r_rxn5 - r_rxn6
    dydt = [d_theta_C2H4_dt, d_theta_H2O_dt, d_theta_2C2H4_dt, d_theta_2H2O_dt, d_theta_C2H4H2O_dt, d_theta_vacant_dt]
    return dydt

C2H4_fractions = np.zeros(len(C2H4_1_egy))
H2O_fractions = np.zeros(len(C2H4_1_egy))
twoC2H4_fractions = np.zeros(len(C2H4_1_egy))
twoH2O_fractions = np.zeros(len(C2H4_1_egy))
C2H4H2O_fractions = np.zeros(len(C2H4_1_egy))
vacant_fractions = np.zeros(len(C2H4_1_egy))
y0 = y=np.array([0, 0, 0, 0, 0 , 1])
t = np.linspace(0,1E10,1E4)       #0,1E5,1E4  --  0,0.1,1E4 (for graphical abstract)
for i in range(len(C2H4_1_egy)):  
    f1 = 6.2014895E5 #adsorption rate constant ethylene on zeolite from collision theory
    f2 = 7.7382354E5 #adsorption rate water from collision theory
    G_1 = y_energies[i,0]
    G_2 = y2_energies[i,0]
    G_3 = y2_energies[i,3]
    G_4 = y_energies[i,1]
    G_5 = y2_energies[i,1]
    G_6 = y_energies[i,3]
    theta_individual = odeint(microkinetic_model, y0, t, args=(6.0E-4, 0.06, f1, f2, G_1, G_2, G_3, G_4, G_5, G_6))
    C2H4_fractions[i] = theta_individual[-1,0]
    H2O_fractions[i] = theta_individual[-1,1]
    twoC2H4_fractions[i] = theta_individual[-1,2]
    twoH2O_fractions[i] = theta_individual[-1,3]
    C2H4H2O_fractions[i] = theta_individual[-1,4]
    vacant_fractions[i] = theta_individual[-1,5] 

#    if (theta_individual[-1,0]-theta_individual[-20,0]) < 0.001:
#         print("Ethylene True")
#    else:
#         print("Ethylene False")
#
#    if(theta_individual[-1,1]-theta_individual[-20,1] < 0.001):
#         print("Water True")
#    else:
#         print("Water False")
#
#    if(theta_individual[-1,5]-theta_individual[-20,5] < 0.001):
#         print("Vacant True")
#    else:
#         print("Vacant False") 

adsorption_fractions = np.column_stack((C2H4_fractions,H2O_fractions))

fig = plt.figure(figsize=(6,4))  #3,3
ax = fig.add_subplot(111)
ax.plot(t, theta_individual[:, 0], 'b', label=r'$\theta_{C_{2}H_{4}}$', linewidth=6)
ax.plot(t, theta_individual[:, 1], 'g', label=r'$\theta_{H_{2}O}$', linewidth=6)
#ax.plot(t, theta_individual[:, 2], 'r', label=r'$2 \theta_{C_{2}H_{4}}$', linewidth=6)
#ax.plot(t, theta_individual[:, 3], 'm', label=r'$2 \theta_{H_{2}O}$', linewidth=6)
#ax.plot(t, theta_individual[:, 4], 'k', label=r'$\theta_{C_{2}H_{4} H_{2}O}$', linewidth=6)
ax.legend(loc='best',fontsize='x-large')
ax.set_title('Microkinetic Model vs Time',  fontsize=20)
ax.set_xlabel('Time (s)', fontsize=20)
ax.set_ylabel(r'Adsorption Fraction',fontsize=20)
fig.savefig('BEEFvdw_MKMvsT.png',dpi=220,bbox_inches="tight")


C2H4_fractions = np.reshape(C2H4_fractions, (2000,1))
H2O_fractions = np.reshape(H2O_fractions, (2000,1)) 
twoC2H4_fractions = np.reshape(twoC2H4_fractions, (2000,1))
twoH2O_fractions = np.reshape(twoH2O_fractions, (2000,1))
C2H4H2O_fractions = np.reshape(C2H4H2O_fractions, (2000,1))
vacant_fractions = np.reshape(vacant_fractions, (2000,1))

#--------------------------------------------------------------------------------------------------
#Stacked Bar Chart
#C2H4_fractions,twoC2H4_fractions,H2O_fractions,twoH2O_fractions,C2H4H2O_fractions,vacant_fractions
AF_array1 = np.hstack((C2H4_fractions, twoC2H4_fractions))
AF_array2 = np.hstack((AF_array1, H2O_fractions))
AF_array3 = np.hstack((AF_array2, twoH2O_fractions))
AF_array4 = np.hstack((AF_array3, C2H4H2O_fractions))
AF_array = np.hstack((AF_array4, vacant_fractions))

AF_array = np.reshape(AF_array, (2000,6))
AF_DF = pd.DataFrame(AF_array, columns=['C2H4_fractions','twoC2H4_fractions','H2O_fractions','twoH2O_fractions','C2H4H2O_fractions','vacant_fractions'])
AF_DF = AF_DF.sort_values(by = 'C2H4_fractions')

totals = [h+i+j+k+l+m for h,i,j,k,l,m in zip(AF_DF['C2H4_fractions'], AF_DF['twoC2H4_fractions'], AF_DF['H2O_fractions'], AF_DF['twoH2O_fractions'], AF_DF['C2H4H2O_fractions'], AF_DF['vacant_fractions'])]
Theta_C2H4 = [i / j * 100 for i,j in zip(AF_DF['C2H4_fractions'], totals)]
Theta_2C2H4 = [i / j * 100 for i,j in zip(AF_DF['twoC2H4_fractions'], totals)]
Theta_H2O = [i / j * 100 for i,j in zip(AF_DF['H2O_fractions'], totals)]
Theta_2H2O = [i / j * 100 for i,j in zip(AF_DF['twoH2O_fractions'], totals)]
Theta_C2H4H2O = [i / j * 100 for i,j in zip(AF_DF['C2H4H2O_fractions'], totals)]
Theta_vacant = [i / j * 100 for i,j in zip(AF_DF['vacant_fractions'], totals)]

N=2000
r=np.arange(N)
print(r)

barwidth=2
fig,ax = plt.subplots()
plt.bar(r,Theta_C2H4, color='b', width=barwidth, label=r'$\theta_{C2H4}$') 
plt.bar(r,Theta_2C2H4, bottom=Theta_C2H4, color='r', width=barwidth, label=r'2 $\theta_{C2H4}$')
plt.bar(r,Theta_H2O, bottom=[i+j for i,j in zip(Theta_2C2H4,Theta_C2H4)], color='g', width=barwidth, label=r'$\theta_{H2O}$')
plt.bar(r,Theta_2H2O, bottom=[i+j+k for i,j,k in zip(Theta_H2O,Theta_2C2H4,Theta_C2H4)], color='m', width=barwidth, label=r'2 $\theta_{H2O}$')
plt.bar(r,Theta_C2H4H2O, bottom=[i+j+k+l for i,j,k,l in zip(Theta_2H2O,Theta_H2O,Theta_2C2H4,Theta_C2H4)], color='y', width=barwidth, label=r'$\theta_{C2H4+H2O}$')
plt.bar(r,Theta_vacant, bottom=[i+j+k+l+m for i,j,k,l,m in zip(Theta_C2H4H2O,Theta_2H2O,Theta_H2O,Theta_2C2H4,Theta_C2H4)], color='k', width=barwidth, label=r'$\theta_{vacant}$')
ax.set_xlabel('Count', fontsize=20)
ax.set_ylabel('Adsorption Site Coverage (%)', fontsize=20)
fig.savefig('AdsFrac_barchart_AllMKM.png', dpi=220)


#----------------------------------------------------------------------
#Matching SUPPORTING INFO Histograms

#MKM
preset_bins = np.linspace(0,1,10)
matplotlib.rc('xtick', labelsize=14)
matplotlib.rc('ytick', labelsize=14)
fig, ax = plt.subplots()
ax.hist([C2H4_fractions, H2O_fractions, twoC2H4_fractions, twoH2O_fractions, C2H4H2O_fractions], bins=preset_bins, label=[r'$\theta_{C2H4}$',r'$\theta_{H2O}$',r'$\theta_{2C2H4}$',r'$\theta_{2H2O}$', r'$\theta_{C2H4+H2O}$'])
ax.set_title('Microkinetic Model Adsorption Fractions',  fontsize=18)
ax.set_ylabel('Frequency', fontsize=18)
ax.set_xlabel('Surface Fraction', fontsize=18)
ax.set_xlim(-.02,1.02)
ax.set_ylim(0,2000)
plt.legend(loc='best', fontsize=16)
fig.savefig('FigS5b.png', dpi=220)


#---------Langmuir Ads Model-------------------------------
K1_L = np.exp(-(C2H4_ads - C2H4_free - zeolite)/(kB*T))
K2_L = np.exp(-(H2O_ads - H2O_free - zeolite)/(kB*T))
P_C2H4 = 0.0006      # Can be 0.0006, 0.00045, 0.0003, 0.0002
P_H2O = 0.06
theta_C2H4_L = np.array(K1_L*P_C2H4/(1 + K1_L*P_C2H4 + K2_L*P_H2O))
theta_H2O_L = np.array(K2_L*P_H2O/(1 + K1_L*P_C2H4 + K2_L*P_H2O))
theta_vacant_L = 1-(theta_C2H4_L+theta_H2O_L)


#Lang
matplotlib.rc('xtick', labelsize=14)
matplotlib.rc('ytick', labelsize=14)
fig, ax = plt.subplots()
ax.hist([theta_C2H4_L, theta_H2O_L], bins=preset_bins, label=[r'$\theta_{C2H4}$',r'$\theta_{H2O}$'])
ax.set_title('Langmuir Isotherm Model Adsorption Fractions', fontsize=18)
ax.set_ylabel('Frequency', fontsize=18)
ax.set_xlabel('Surface Fraction', fontsize=18)
ax.set_xlim(-.02,1.02)
ax.set_ylim(0,2000)
plt.legend(loc='upper center', fontsize=16)    #'upper right'
fig.savefig('FigS5a.png', dpi=220)


