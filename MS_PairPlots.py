#!/usr/bin/env python
# coding: utf-8
# In[30]:
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
matplotlib.use('Agg')
import sklearn
from sklearn.linear_model import LinearRegression
import seaborn as sns; sns.set(style="ticks", color_codes=True)
from scipy.integrate import odeint
from scipy import stats


kB = 8.617E-5
T=298.15
H2O_free = pd.read_csv('BEEF_vdw_ensemble_energies_H2O_free.csv', header=None) + 0.6034 - 0.58352 #+ZPE - entropy
C2H4_free = pd.read_csv('BEEF_vdw_ensemble_energies_C2H4_free.csv', header=None) + 1.3886 - 0.67770
H2O_ads = pd.read_csv('BEEF_vdw_H2O-1.csv', header=None) + 0.682 - 0.629
C2H4_ads = pd.read_csv('BEEF_vdw_C2H4-1.csv', header=None) + 1.43 - 1.34
zeolite = pd.read_csv('BEEF_vdw_Zeolite.csv', header=None)
K1 = np.exp(-(C2H4_ads - C2H4_free - zeolite)/(kB*T))
K2 = np.exp(-(H2O_ads - H2O_free - zeolite)/(kB*T))
#-----------------------------------------
#PBE
#make PBE red:
C2H4_ads_PBE1 = -887.2347096 + 1.34
C2H4_ads_PBE2 = -919.2696946 + 2*(1.34)
C2H4_ads_PBE3 = -952.0451751 + 3*(1.34)
C2H4_free_PBE = -31.9634663 + 1.39 - 0.678

H2O_ads_PBE1 = -869.3338647 + 0.629
H2O_ads_PBE2 = -884.10898 + 2*(0.629)
H2O_ads_PBE3 = -898.8088181 + 3*(0.629)
H2O_free_PBE = -14.2209464 + 0.603 - 0.584

zeolite_PBE = -854.10658

#-----------------------------------------
#HSE
#HSE make green:
C2H4_ads_HSE1 = -1211.12274 + 1.34
C2H4_ads_HSE2 = -1249.249413 + 2*(1.34)
C2H4_ads_HSE3 = -1288.034834 + 3*(1.34)
C2H4_free_HSE = -38.07780615 +1.39 - 0.678

H2O_ads_HSE1 = -1191.798217 + 0.629
H2O_ads_HSE2 = -1211.214083 + 2*(0.629)
H2O_ads_HSE3 = -1230.591463 + 3*(0.629)
H2O_free_HSE = -18.87323798 + 0.603 - 0.584

zeolite_HSE = -1171.9988

#----------------------------------------------------------------------------------------
#C2H4 DFT

C2H4_1_egy = np.genfromtxt('BEEF_vdw_C2H4-1.csv') + 1.43 - 1.34
C2H4_2_egy = np.genfromtxt('BEEF_vdw_C2H4-2.csv') + 0.528 - 0.301
C2H4_3_egy = np.genfromtxt('BEEF_vdw_C2H4-3.csv') + 0.279 - (-0.00861)
C2H4_free_np = np.genfromtxt('BEEF_vdw_ensemble_energies_C2H4_free.csv') + 1.3886 - 0.67770
zeolite_np = np.genfromtxt('BEEF_vdw_Zeolite.csv')

H2O_1_egy = np.genfromtxt('BEEF_vdw_H2O-1.csv') + 0.682 - 0.629
H2O_2_egy = np.genfromtxt('BEEF_vdw_H2O-2.csv') + 1.35 - 1.24
H2O_3_egy = np.genfromtxt('BEEF_vdw_H2O-3.csv') + 0.0896 - (-0.073)
H2O_free_np = np.genfromtxt('BEEF_vdw_ensemble_energies_H2O_free.csv') + 0.6034 - 0.58352
zeolite_np = np.genfromtxt('BEEF_vdw_Zeolite.csv')

#-----------
#PBE
y_energiesPBE = np.zeros((50,3))
y_energiesPBE[:,0] = C2H4_ads_PBE1 - C2H4_free_PBE - zeolite_PBE
y_energiesPBE[:,1] = C2H4_ads_PBE2 - C2H4_ads_PBE1 - C2H4_free_PBE
y_energiesPBE[:,2] = C2H4_ads_PBE3 - C2H4_ads_PBE2 - C2H4_free_PBE 
C2H4_engPBE = np.concatenate(y_energiesPBE).reshape((50,3))
print(y_energiesPBE)
print(y_energiesPBE.shape)
#-----------
#HSE
y_energiesHSE = np.zeros((50,3))                                                      #1,3
y_energiesHSE[:,0] = C2H4_ads_HSE1 - C2H4_free_HSE - zeolite_HSE                        #[0,0]
y_energiesHSE[:,1] = C2H4_ads_HSE2 - C2H4_ads_HSE1 - C2H4_free_HSE                      #[0,1]
y_energiesHSE[:,2] = C2H4_ads_HSE3 - C2H4_ads_HSE2 - C2H4_free_HSE                      #[0,2]
C2H4_engHSE = np.concatenate(y_energiesHSE).reshape((50,3))                   #1,3
print(y_energiesHSE)
print(y_energiesHSE.shape)

#y_energies is delta G

y_energies = np.zeros((len(C2H4_1_egy),3))
y_energies[:,0] = C2H4_1_egy - C2H4_free_np - zeolite_np
y_energies[:,1] = C2H4_2_egy - C2H4_1_egy - C2H4_free_np 
y_energies[:,2] = C2H4_3_egy - C2H4_2_egy - C2H4_free_np
C2H4_eng = np.concatenate(y_energies).reshape((2000,3))
print(C2H4_eng)
print(C2H4_eng.shape)

#------------------------------------------------------------------------
#PairPlot

DFT2 = np.vstack((C2H4_eng, y_energiesPBE))
DFT3 = np.vstack((DFT2, y_energiesHSE))

DFT3_2col = DFT3[:,:2]

print(DFT3_2col)
print(DFT3_2col.shape)

#DFT3['BEEF_vdw'] = DFT3[0:-2,:]
#DFT3['PBE'] = DFT3[-2,:]
#DFT3['HSE'] = DFT3[-1,:]

#index = np.empty([2002, 1], dtype=str)
#index1 = np.zeros((4000,1))                                #2002,1
index0 = np.zeros((2000,1))
#index[:,0] = 'BEEF_vdw'
#index[-2,0] = 'PBE'
#index[-1,0] = 'HSE' 
a = np.array([[1],[2]])
index1 = np.repeat(a, 50)
index1 = np.reshape(index1, (100, 1))
index = np.vstack((index0, index1))
#index[-2,0] = 1
#index[-1,0] = 2 

print(index)
print(index.shape)

#DFT_all = np.hstack((DFT3.astype(float),index.astype(str)))
DFT_all = np.hstack((DFT3_2col,index))
print(DFT_all)
print(DFT_all.shape)

C2H4pd_eng = pd.DataFrame(np.array(DFT_all), columns=[r'$^* + C_2H_4(g) \leftrightarrow C_2H_4^*$', r'$C_2H_4^* + C_2H_4(g) \leftrightarrow 2C_2H_4^*$', 'DFT Type'])
#C2H4pd_PBE = pd.DataFrame(np.array(y_energiesPBE), index=['BEEF_vdw' 'PBE', 'HSE'], columns=['$G_{rxn1}$ (eV)', '$G_{rxn2}$ (eV)', '$G_{rxn3}$ (eV)'])
#full = pd.concat([C2H4_eng, C2H4pd_PBE], ignore_index=True)

print(C2H4pd_eng)


sns.set(style="ticks", font_scale=1)
C2H4_PairPlot = sns.pairplot(C2H4pd_eng, vars=[r'$^* + C_2H_4(g) \leftrightarrow C_2H_4^*$', r'$C_2H_4^* + C_2H_4(g) \leftrightarrow 2C_2H_4^*$'], hue="DFT Type",  markers=["o", "s", "x"], diag_kind='hist')  
C2H4_PairPlot.set(xlim=(-3,2))
C2H4_PairPlot.set(ylim=(-3,2))
#C2H4_PairPlot.fig.suptitle("C2H4 Energies")
C2H4_PairPlot = C2H4_PairPlot.add_legend(fontsize=20)

C2H4_PairPlot.savefig('PairPlotC2H4.png',dpi=220)


#-----------------------------------------------------------------------
print(C2H4_ads - C2H4_free - zeolite)  #same as y[:,0]
#------------------------------------------------------------------------


#H2O
#-----------
#PBE
y2_energiesPBE = np.zeros((50,3))
y2_energiesPBE[:,0] = H2O_ads_PBE1 - H2O_free_PBE - zeolite_PBE
y2_energiesPBE[:,1] = H2O_ads_PBE2 - H2O_ads_PBE1 - H2O_free_PBE
y2_energiesPBE[:,2] = H2O_ads_PBE3 - H2O_ads_PBE2 - H2O_free_PBE
#H2O_engPBE = np.concatenate(y_energiesPBE).reshape((1,3))
print(y2_energiesPBE)
print(y2_energiesPBE.shape)
#-----------
#HSE
y2_energiesHSE = np.zeros((50,3))
y2_energiesHSE[:,0] = H2O_ads_HSE1 - H2O_free_HSE - zeolite_HSE
y2_energiesHSE[:,1] = H2O_ads_HSE2 - H2O_ads_HSE1 - H2O_free_HSE
y2_energiesHSE[:,2] = H2O_ads_HSE3 - H2O_ads_HSE2 - H2O_free_HSE
#H2O_engHSE = np.concatenate(y2_energiesHSE).reshape((1,3))
print(y2_energiesHSE)
print(y2_energiesHSE.shape)

#y_energies is delta G
y2_energies = np.zeros((len(H2O_1_egy),3))
y2_energies[:,0] = H2O_1_egy - H2O_free_np - zeolite_np
y2_energies[:,1] = H2O_2_egy - H2O_1_egy - H2O_free_np
y2_energies[:,2] = H2O_3_egy - H2O_2_egy -  H2O_free_np

print(H2O_ads - H2O_free - zeolite) #same as y[:,0]

H2O_eng = np.concatenate(y2_energies).reshape((2000,3))
print(H2O_eng)
print(H2O_eng.shape)

DFT2_H2O = np.vstack((H2O_eng, y2_energiesPBE))
DFT3_H2O = np.vstack((DFT2_H2O, y2_energiesHSE))
print(DFT3_H2O)
print(DFT3_H2O.shape)

DFT3_H2O_2col = DFT3_H2O[:,:2]

DFT2_all = np.hstack((DFT3_H2O_2col,index))
print(DFT2_all)
print(DFT2_all.shape)

#-----------------------------------------------------------------------
#PairPlot


H2Opd_eng = pd.DataFrame(np.array(DFT2_all), columns=[r'$^* + H_2O(g) \leftrightarrow H_2O^*$', r'$H_2O^* + H_2O(g) \leftrightarrow 2H_2O^*$', 'DFT Type'])
#H2O_PairPlot = sns.pairplot(H2Opd_eng, vars=[r'$^* + H_2O(g) \leftrightarrow H_2O^*$', r'$H_2O^* + H_2O(g) \leftrightarrow 2H_2O^*$'], hue="DFT Type", markers=["o", "s", "x"], diag_kind='hist')
H2O_PairPlot = sns.pairplot(H2Opd_eng, vars=[r'$^* + H_2O(g) \leftrightarrow H_2O^*$', r'$H_2O^* + H_2O(g) \leftrightarrow 2H_2O^*$'], hue="DFT Type", markers=["o", "s", "x"], diag_kind='hist')
H2O_PairPlot.set(xlim=(-2,2.5))
H2O_PairPlot.set(ylim=(-2,2.5))
H2O_PairPlot = H2O_PairPlot.add_legend(fontsize=20)
H2O_PairPlot.savefig('PairPlotH2O.png', dpi=220)

fig, ax = plt.subplots()
x1 = np.array(y2_energies[:,0])
print(x1)
print(x1.size)
ax = sns.distplot(x1, norm_hist=True, kde=True)
fig.savefig('distplot_H2O-1', dpi=220)



#preset_bins = np.linspace(-3,3,1000)
#matplotlib.rc('xtick', labelsize=14)
#matplotlib.rc('ytick', labelsize=14)
#fig, ax = plt.subplots()
#ax.hist(y2_energies[:,0], bins=preset_bins, normed=True)
#ax.set_ylabel('Probability Density', fontsize=18)
#ax.set_xlabel(r'$^* + H_2O(g) \leftrightarrow H_2O^*$', fontsize=18)
#plt.legend(loc='best', fontsize=16)
#fig.savefig('PairPlotH20_Hist.png', dpi=220)
