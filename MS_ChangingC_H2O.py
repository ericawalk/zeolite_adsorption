import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
matplotlib.use('Agg')
import sklearn
from sklearn.linear_model import LinearRegression
import seaborn as sns; sns.set(style="ticks", color_codes=True)
from scipy.integrate import odeint
import scipy.stats as st
from scipy.stats import sem, t
from scipy import mean
import pickle
matplotlib.rcParams.update({'font.size': 26})

tot_C2H4_BEEF = pickle.load(open("tot_C2H4_BEEF.p", "rb"))
tot_H2O_BEEF = pickle.load( open("tot_H2O_BEEF.p", "rb"))
vacant_frac_BEEF = pickle.load(open("vacant_frac_BEEF.p", "rb"))
divisor = tot_C2H4_BEEF + tot_H2O_BEEF + vacant_frac_BEEF
percent_H2O = tot_H2O_BEEF/divisor * 100
percent_H2O_sorted = np.sort(percent_H2O,axis=0)
print(tot_C2H4_BEEF)
print(tot_H2O_BEEF)
print(vacant_frac_BEEF)
print(divisor)
print(tot_H2O_BEEF/divisor)
print(percent_H2O_sorted)

figEAW, axEAW = plt.subplots(figsize=(5,4))
feed_C2H4 = np.array([600, 450, 300, 200])
axEAW.plot(feed_C2H4, percent_H2O_sorted[999,:], 'b', label=r'mean', linewidth=6)
axEAW.plot(feed_C2H4, percent_H2O_sorted[49,:], 'g--', label=r'95% confidence', linewidth=6)
axEAW.plot(feed_C2H4, percent_H2O_sorted[1949,:], 'g--', linewidth=6)
#axEAW.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)
axEAW.legend(loc='best', bbox_to_anchor=(0.9,0.5))
axEAW.set_xlabel(r'$C_{2}H_{4}$ Feed Gas Concentration (ppm)')
axEAW.set_ylabel(r'Adsorption Site Coverage $H_{2}O$ (%)')
axEAW.set_yscale('log')
axEAW.set_xlim([200,600])
figEAW.savefig('ChangingC_H2O_conf_int.png',dpi=220,bbox_inches="tight")

#figEAW_1, axEAW_1 = plt.subplots()
#axEAW_1.plot(feed_C2H4, percent_H2O_sorted[999,:], 'b', label=r'mean', linewidth=6)
#axEAW_1.set_xlabel(r'$C_{2}H_{4}$ Feed Gas Concentration (ppm)')
#axEAW_1.set_ylabel(r'Adsorption Site Coverage $H_{2}O$ (%)')
#axEAW_1.legend(loc='best')
#figEAW_1.savefig('ChangingC_EAW_H2O_mean.png',dpi=220,bbox_inches="tight")
