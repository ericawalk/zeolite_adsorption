#!/usr/bin/env python
# coding: utf-8
# In[30]:
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
matplotlib.use('Agg')
import sklearn
from sklearn.linear_model import LinearRegression
import seaborn as sns; sns.set(style="ticks", color_codes=True)
from scipy.integrate import odeint
import statsmodels.formula.api as smf


kB = 8.617E-5
T=298.15
H2O_free = pd.read_csv('BEEF_vdw_ensemble_energies_H2O_free.csv', header=None) + 0.6034 - 0.58352 #+ZPE - entropy
C2H4_free = pd.read_csv('BEEF_vdw_ensemble_energies_C2H4_free.csv', header=None) + 1.3886 - 0.67770
H2O_ads = pd.read_csv('BEEF_vdw_H2O-1.csv', header=None) + 0.682 - 0.629
C2H4_ads = pd.read_csv('BEEF_vdw_C2H4-1.csv', header=None) + 1.43 - 1.34
zeolite = pd.read_csv('BEEF_vdw_Zeolite.csv', header=None)
#-----------------------------------------
#PBE
C2H4_ads_PBE1 = -887.2347096 + 1.34
C2H4_ads_PBE2 = -919.2696946 + 2*(1.34)
C2H4_ads_PBE3 = -952.0451751 + 3*(1.34)
C2H4_free_PBE = -31.9634663 + 1.39 - 0.678

H2O_ads_PBE1 = -869.3338647 + 0.629
H2O_ads_PBE2 = -884.10898 + 2*(0.629)
H2O_ads_PBE3 = -898.8088181 + 3*(0.629)
H2O_free_PBE = -14.2209464 + 0.603 - 0.584

zeolite_PBE = -854.10658
#-----------------------------------------
#HSE
C2H4_ads_HSE1 = -1211.12274 + 1.34
C2H4_ads_HSE2 = -1249.249413 + 2*(1.34)
C2H4_ads_HSE3 = -1288.034834 + 3*(1.34)
C2H4_free_HSE = -38.07780615 +1.39 - 0.678

H2O_ads_HSE1 = -1191.798217 + 0.629
H2O_ads_HSE2 = -1211.214083 + 2*(0.629)
H2O_ads_HSE3 = -1230.591463 + 3*(0.629)
H2O_free_HSE = -18.87323798 + 0.603 - 0.584

zeolite_HSE = -1171.9988
#---------------------------------------------------------------------------------------
# ************* C2H4 AND H2O***************
C2H4_H2O_PBE = -902.0079807 + 0.512
C2H4_H2O_HSE = -1230.557244 + 0.512
C2H4_H2O_BEEFvdw = np.genfromtxt('BEEF_vdw_C2H4-H2O.csv') + 0.674 - 0.512
# ***********************************************
#----------------------------------------------------------------------------------------
#BEEFvdw
C2H4_1_egy = np.genfromtxt('BEEF_vdw_C2H4-1.csv') + 1.43 - 1.34
C2H4_2_egy = np.genfromtxt('BEEF_vdw_C2H4-2.csv') + 0.528 - 0.301
C2H4_3_egy = np.genfromtxt('BEEF_vdw_C2H4-3.csv') + 0.279 - (-0.00861)
C2H4_free_np = np.genfromtxt('BEEF_vdw_ensemble_energies_C2H4_free.csv') + 1.3886 - 0.67770

H2O_1_egy = np.genfromtxt('BEEF_vdw_H2O-1.csv') + 0.682 - 0.629
H2O_2_egy = np.genfromtxt('BEEF_vdw_H2O-2.csv') + 1.35 - 1.24
H2O_3_egy = np.genfromtxt('BEEF_vdw_H2O-3.csv') + 0.0896 - (-0.073)
H2O_free_np = np.genfromtxt('BEEF_vdw_ensemble_energies_H2O_free.csv') + 0.6034 - 0.58352

zeolite_np = np.genfromtxt('BEEF_vdw_Zeolite.csv')
#-----------------------------------------------------------------------------------------

#y_energies is delta G
y_energies = np.zeros((len(C2H4_1_egy),4))
y_energies[:,0] = C2H4_1_egy - C2H4_free_np - zeolite_np
y_energies[:,1] = C2H4_2_egy - C2H4_1_egy - C2H4_free_np 
y_energies[:,2] = C2H4_3_egy - C2H4_2_egy - C2H4_free_np
y_energies[:,3] = C2H4_H2O_BEEFvdw - H2O_1_egy - C2H4_free_np

y2_energies = np.zeros((len(H2O_1_egy),4))
y2_energies[:,0] = H2O_1_egy - H2O_free_np - zeolite_np
y2_energies[:,1] = H2O_2_egy - H2O_1_egy - H2O_free_np
y2_energies[:,2] = H2O_3_egy - H2O_2_egy -  H2O_free_np
y2_energies[:,3] = C2H4_H2O_BEEFvdw - C2H4_1_egy - H2O_free_np
#---------------------
C2H4_eng = np.concatenate(y_energies).reshape((2000,4))
#---------------------
H2O_eng = np.concatenate(y2_energies).reshape((2000,4))
#---------------------------------------------------------------------------------------

#---------Langmuir Ads Model--------------
K1_L = np.exp(-(C2H4_ads - C2H4_free - zeolite)/(kB*T))
K2_L = np.exp(-(H2O_ads - H2O_free - zeolite)/(kB*T))

P_C2H4 = 0.0006      # Can be 0.0006, 0.00045, 0.0003, 0.0002
P_H2O = 0.06

theta_C2H4_L = np.array(K1_L*P_C2H4/(1 + K1_L*P_C2H4 + K2_L*P_H2O))
theta_H2O_L = np.array(K2_L*P_H2O/(1 + K1_L*P_C2H4 + K2_L*P_H2O))
theta_vacant_L = 1-(theta_C2H4_L+theta_H2O_L)


#-------------------------------------------------------------------------------------
#Stacked bar plot
#C2H4_fractions,twoC2H4_fractions,H2O_fractions,twoH2O_fractions,C2H4H2O_fractions,vacant_fractions

AF_array1 = np.hstack((theta_C2H4_L, theta_H2O_L))
AF_array = np.hstack((AF_array1, theta_vacant_L))
print(AF_array)
print(AF_array.shape)
AF_array = np.reshape(AF_array, (2000,3))
AF_DF = pd.DataFrame(AF_array, columns=['C2H4_fractions','H2O_fractions','vacant_fractions'])

#SORT ARRAY
AF_DF = AF_DF.sort_values(by = 'C2H4_fractions')

totals = [i+j+k for i,j,k in zip(AF_DF['C2H4_fractions'], AF_DF['H2O_fractions'], AF_DF['vacant_fractions'])]
Theta_C2H4 = [i / j * 100 for i,j in zip(AF_DF['C2H4_fractions'], totals)]
Theta_H2O = [i / j * 100 for i,j in zip(AF_DF['H2O_fractions'], totals)]
Theta_vacant = [i / j * 100 for i,j in zip(AF_DF['vacant_fractions'], totals)]

N=2000
r=np.arange(N)
print(r)

barwidth=2
fig = plt.figure(figsize=(5,4.5))
ax = fig.add_subplot(111)
plt.bar(r,Theta_C2H4, color='b', width=barwidth, label=r'$\theta_{C2H4}$') 
plt.bar(r,Theta_H2O, bottom=Theta_C2H4, color='g', width=barwidth, label=r'$\theta_{H2O}$')
plt.bar(r,Theta_vacant, bottom=[i+j for i,j in zip(Theta_H2O,Theta_C2H4)], color='k', width=barwidth, label=r'$\theta_{vacant}$')
ax.set_xlabel('Count', fontsize=12)
ax.set_ylabel('Adsorption Site Coverage (%)', fontsize=12)
ax.legend(loc='lower left', bbox_to_anchor=(-0.01,1.0,1,0.2), ncol=3, fontsize='large')
fig.savefig('AdsFrac_barchart_Lang.png', dpi=220)



